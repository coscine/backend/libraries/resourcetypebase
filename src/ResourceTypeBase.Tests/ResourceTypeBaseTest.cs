﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeBase.Tests
{
    [TestFixture]
    public class ResourceTypeBaseTest
    {
        private readonly string resourceTypeName = "rds";

        private readonly string prefix = "prefix";
        private readonly string key = "key";
        private readonly string keyNew = "keyNew";
        private readonly string id = "00000000-0000-0000-0000-000000000000";
        private readonly int quota = 0;
        private readonly Dictionary<string, string> properties = new Dictionary<string, string>();


        private readonly string entyName = "abc";
        private readonly bool hasBody = true;
        private readonly string[] versions = new string[1] { "1.0.0" };
        private readonly long bodyBytes = 0;
        private readonly string pid = "00000000-0000-0000-0000-000000000000";
        private readonly DateTime created = new DateTime();
        private readonly DateTime modified = new DateTime();

        [OneTimeSetUp]
        public void Setup()
        {

        }

        [OneTimeTearDown]
        public void End()
        {
        }

        [Test]
        public void TestConstructorResourceEntry()
        {
            _ = new ResourceEntry(entyName, hasBody, bodyBytes, pid, versions, created, modified);
        }

        [Test]
        public void TestConstructorResourceTypeDefinition()
        {
            _ = new ResourceTypeDefinition(resourceTypeName, null, null);
        }

        [Test]
        public async Task TestConstructorResourceTypeDefinitionEntry()
        {
            var resourceTypeDefinition = new ResourceTypeDefinition(resourceTypeName, null, null);
            await resourceTypeDefinition.ListEntries(id, prefix);
            await resourceTypeDefinition.GetEntry(id, key);
            await resourceTypeDefinition.StoreEntry(id, key, null);
            await resourceTypeDefinition.RenameEntry(id, key, keyNew);
            await resourceTypeDefinition.DeleteEntry(id, key);
            await resourceTypeDefinition.LoadEntry(id, key);
            await resourceTypeDefinition.AddPrefix(id, prefix);
            await resourceTypeDefinition.GetEntryStoreUrl(key);
            await resourceTypeDefinition.GetEntryDownloadUrl(key);
        }

        [Test]
        public async Task TestConstructorResourceTypeDefinitionResourceAsync()
        {
            var resourceTypeDefinition = new ResourceTypeDefinition(resourceTypeName, null, null);
            await resourceTypeDefinition.CreateResource();
            await resourceTypeDefinition.UpdateResource(id);
            await resourceTypeDefinition.IsResourceCreated(id);
            await resourceTypeDefinition.DeleteResource(id);
            await resourceTypeDefinition.SetResourceQuota(id, quota);
            await resourceTypeDefinition.GetResourceQuotaAvailable(id);
            await resourceTypeDefinition.GetResourceQuotaUsed(id);
        }
    }
}
