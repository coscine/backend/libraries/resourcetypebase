﻿using System;
using System.Collections.Generic;

namespace Coscine.ResourceTypeBase
{
    /// <summary>
    /// Object containing all relevant information about the resource type for the frontend.
    /// </summary>
    [Serializable]
    public class ResourceTypeInformation
    {
        /// <summary>
        /// The resource type is enabled.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// The resource type is archived.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Value to see, if the resource has a quota.
        /// </summary>
        public bool IsQuotaAvailable { get; set; }

        /// <summary>
        /// Value to see, if the resource quota can be chagned.
        /// </summary>
        public bool IsQuotaAdjustable { get; set; }

        /// <summary>
        /// Display name of the resource type.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Guid of the resource type.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Object containing the relevant information about the resource type for the resource create page.
        /// </summary>
        public ResourceCreateObject ResourceCreate { get; set; } = new ResourceCreateObject();

        /// <summary>
        /// Object containing the relevant information about the resource type for the resource content page.
        /// </summary>
        public ResourceContentObject ResourceContent { get; set; } = new ResourceContentObject();

        /// <summary>
        /// Standard constructor.
        /// </summary>
        public ResourceTypeInformation()
        {

        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="id">The Guid of the resource type.</param>
        /// <param name="displayName">Displayname of the resource.</param>
        /// <param name="isEnabled">The resource is enabled.</param>
        /// /// <param name="resourceCreateObj">Object containing the relevant information about the resource type for the resource create page.</param>
        /// /// <param name="resourceContentObj">Object containing the relevant information about the resource type for the resource content page.</param>
        public ResourceTypeInformation(Guid id, string displayName, bool isEnabled, ResourceCreateObject resourceCreateObj, ResourceContentObject resourceContentObj)
        {
            IsEnabled = isEnabled;
            DisplayName = displayName;
            Id = id;
            ResourceCreate = resourceCreateObj;
            ResourceContent = resourceContentObj;
        }


    }

    /// <summary>
    /// Object containing the relevant information about the resource type for the resource create page.
    /// </summary>
    [Serializable]
    public class ResourceCreateObject
    {
        /// <summary>
        /// List of Lists containing all the resource type specific components for the steps in the resource creation page.
        /// </summary>
        public List<List<string>> Components { get; set; }

        /// <summary>
        /// Standard constructor.
        /// </summary>
        public ResourceCreateObject()
        {
            Components = new List<List<string>>();
            for (var i = 0; i < 4; i++)
            {
                Components.Add(new List<string>());
            }
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="components">List of Lists containing all the resource type specific components for the steps in the resource creation page.</param>
        public ResourceCreateObject(List<List<string>> components)
        {
            Components = components;
        }
    }

    /// <summary>
    /// Object containing the relevant information about the resource type for the resource content page.
    /// </summary>
    [Serializable]
    public class ResourceContentObject
    {
        /// <summary>
        /// Resource is readonly.
        /// </summary>
        public bool ReadOnly { get; set; } = false;

        /// <summary>
        /// Object containing the relevant information about the resource type for the metadata vue component.
        /// </summary>
        public MetadataView MetadataView { get; set; } = new MetadataView();

        /// <summary>
        /// Object containing the relevant information about the resource type for the entriesview vue component.
        /// </summary>
        public EntriesView EntriesView { get; set; } = new EntriesView();

        /// <summary>
        /// Standard constructor.
        /// </summary>
        public ResourceContentObject()
        {

        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="resourceIsReadonly">Resource is readonly.</param>
        /// <param name="metadataViewObj">Object containing the relevant information about the resource type for the metadata vue component.</param>
        /// <param name="entriesViewObj">Object containing the relevant information about the resource type for the entriesview vue component.</param>
        public ResourceContentObject(bool resourceIsReadonly, MetadataView metadataViewObj, EntriesView entriesViewObj)
        {
            ReadOnly = resourceIsReadonly;
            MetadataView = metadataViewObj;
            EntriesView = entriesViewObj;
        }
    }

    /// <summary>
    /// Object containing the relevant information about the resource type for the metadata vue component.
    /// </summary>
    [Serializable]
    public class MetadataView
    {
        /// <summary>
        /// A dataUrl canbe provided.
        /// </summary>
        public bool EditableDataUrl { get; set; } = false;

        /// <summary>
        /// A key can be provided.
        /// </summary>
        public bool EditableKey { get; set; } = false;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        public MetadataView()
        {

        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="editableDataUrl">A dataUrl canbe provided.</param>
        /// <param name="editableKey">A key can be provided.</param>
        public MetadataView(bool editableDataUrl, bool editableKey)
        {
            EditableDataUrl = editableDataUrl;
            EditableKey = editableKey;
        }

    }

    /// <summary>
    /// Object containing the relevant information about the resource type for the entriesview vue component.
    /// </summary>
    [Serializable]
    public class EntriesView
    {
        /// <summary>
        /// Object containing the relevant information about the resource type for the columns within entriesview vue component.
        /// </summary>
        public ColumnsObject Columns { get; set; } = new ColumnsObject();

        /// <summary>
        /// Standard constructor.
        /// </summary>
        public EntriesView()
        {

        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="columns">Object containing the relevant information about the resource type for the columns within entriesview vue component.</param>
        public EntriesView(ColumnsObject columns)
        {
            Columns = columns;
        }
    }

    /// <summary>
    /// Object containing the relevant information about the resource type for the columns within entriesview vue component.
    /// </summary>
    [Serializable]
    public class ColumnsObject
    {
        /// <summary>
        /// List of columns that should always be displayed in the entries view.
        /// </summary>
        public List<string> Always { get; set; } = new List<string>() { "name", "size" };

        /// <summary>
        /// Standard constructor.
        /// </summary>
        public ColumnsObject()
        {

        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="always">List of columns that should always be displayed in the entries view.</param>
        public ColumnsObject(List<string> always)
        {
            Always = always;
        }
    }
}
