﻿using System.Collections.Generic;

namespace Coscine.ResourceTypeBase
{
    public class ResourceTypeConfigurationObject
    {
        public Dictionary<string, string> Name { get; set; }
        public string Path { get; set; }
        public string ClassName { get; set; }
        public string Status { get; set; }
        public Dictionary<string, string> Config { get; set; }
        public List<string> RequiredFields { get; set; }
    }
}
