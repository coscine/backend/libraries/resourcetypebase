﻿using System;

namespace Coscine.ResourceTypeBase
{
    public class ResourceEntry
    {
        public string Key { get; set; }

        public bool HasBody { get; set; }

        public long BodyBytes { get; set; }

        public string Pid { get; set; }

        public string[] Versions { get; set; }

        public DateTime? Modified { get; set; }

        public DateTime? Created { get; set; }        

        public ResourceEntry(string key, bool hasBody, long bodyBytes, string pid, string[] versions, DateTime created, DateTime modified)
        {
            Key = key;
            HasBody = hasBody;
            BodyBytes = bodyBytes;
            Pid = pid;
            Versions = versions;
            Created = created;
            Modified = modified;
        }
    }
}
