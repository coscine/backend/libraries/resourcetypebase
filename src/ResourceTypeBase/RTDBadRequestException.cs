﻿using System;

namespace Coscine.ResourceTypeBase
{
    public class RTDBadRequestException : Exception
    {
        public RTDBadRequestException()
        {
        }

        public RTDBadRequestException(string message)
            : base(message)
        {
        }

        public RTDBadRequestException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
