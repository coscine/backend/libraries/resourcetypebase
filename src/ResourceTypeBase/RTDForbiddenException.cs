﻿using System;

namespace Coscine.ResourceTypeBase
{
    public class RTDForbiddenException : Exception
    {
        public RTDForbiddenException()
        {
        }

        public RTDForbiddenException(string message)
            : base(message)
        {
        }

        public RTDForbiddenException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
