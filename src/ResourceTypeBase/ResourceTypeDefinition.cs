﻿using Coscine.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeBase
{
    public class ResourceTypeDefinition
    {
        public string Name { get; set; }

        public ResourceTypeConfigurationObject ResourceTypeConfiguration { get; set; }

        public IConfiguration Configuration { get; set; }

        public ResourceTypeDefinition(string name, IConfiguration gConfig, ResourceTypeConfigurationObject resourceTypeConfiguration)
        {
            Name = name;
            Configuration = gConfig;
            ResourceTypeConfiguration = resourceTypeConfiguration;
        }

        public virtual Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string> options = null)
        {
            return Task.FromResult(new List<ResourceEntry>());
        }

        public virtual Task<ResourceEntry> GetEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            return Task.FromResult<ResourceEntry>(null);
        }

        public virtual Task StoreEntry(string id, string key, Stream body, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task RenameEntry(string id, string keyOld, string keyNew, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task DeleteEntry(string id, string key, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task<Stream> LoadEntry(string id, string key, string verion = null, Dictionary<string, string> options = null)
        {
            return Task.FromResult<Stream>(null);
        }

        public virtual Task AddPrefix(string id, string prefix, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task CreateResource(Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task UpdateResource(string id, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task<bool> IsResourceCreated(string id, Dictionary<string, string> options = null)
        {
            return Task.FromResult(false);
        }

        public virtual Task DeleteResource(string id, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task<ResourceTypeInformation> GetResourceTypeInformation()
        {
            var resourceTypeInformation = new ResourceTypeInformation 
            {
                IsEnabled = ResourceTypeConfiguration.Status == "active",
                DisplayName = Name
            };

            var components = new List<List<string>>();
            for (var i = 0; i < 4; i++)
            {
                components.Add(new List<string>());
            }
            components[0] = ResourceTypeConfiguration.RequiredFields;

            resourceTypeInformation.ResourceCreate.Components = components;
            return Task.FromResult(resourceTypeInformation);
        }

        public virtual Task SetResourceQuota(string id, int quota, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public virtual Task<long> GetResourceQuotaAvailable(string id, Dictionary<string, string> options = null)
        {
            return Task.FromResult<long>(-1);
        }

        public virtual Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string> options = null)
        {
            return Task.FromResult<long>(0);
        }

        public virtual Task<Uri> GetEntryStoreUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return Task.FromResult<Uri>(null); ;
        }

        public virtual Task<Uri> GetEntryDownloadUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return Task.FromResult<Uri>(null); ;
        }

        public virtual Task SetResourceReadonly(string id, bool status, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }
    }
}
