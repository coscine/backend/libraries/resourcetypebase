﻿using System;

namespace Coscine.ResourceTypeBase
{
    public class RTDNotFoundException : Exception
    {
        public RTDNotFoundException()
        {
        }

        public RTDNotFoundException(string message)
            : base(message)
        {
        }

        public RTDNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
